/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxoop;

/**
 *
 * @author User
 */
public class Player {

    private char symbol;
    private int win;
    private int loss;
    private int draw;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", win=" + win + ", loss=" + loss + ", draw=" + draw + '}';
    }

    public char setSymbol(char symbol) {
        return symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getWin() {
        return win;
    }

    public void Win() {
        this.win++;
    }

    public int getLoss() {
        return loss;
    }

    public void Loss() {
        this.loss++;
    }

    public int getDraw() {
        return draw;
    }

    public void Draw() {
        this.draw++;
    }
}
